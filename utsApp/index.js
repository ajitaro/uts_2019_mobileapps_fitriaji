// import {AppRegistry} from 'react-native';
import App from './App';
import App2 from './App2';
import SideMenu from './screens/SideMenu'
import SayakoMainMenu from './screens/SayakoMainMenu';
// import {name as appName} from './app.json';
import {Navigation} from "react-native-navigation";
import RegisterAndLogin from "./screens/RegisterAndLogin";
import HomePage from "./screens/Home/HomePage";
import OrderPage from "./screens/OrderPage";
import EditPage from "./screens/Edit/EditPage";
import ProfilePage from "./screens/ProfilePage";
import AddItem from "./screens/Edit/AddItem";
import LoginPage from "./screens/LoginAndRegister/LoginPage";
import BenefitPage from "./screens/Home/BenefitPage";
import RegisterPage from "./screens/LoginAndRegister/RegisterPage";
import TransferHistoryPage from "./screens/Home/TransferHistory/TransferHistoryPage";
import HistoryPage from "./screens/Home/HistoryPage";



// AppRegistry.registerComponent(appName, () => App);

Navigation.registerComponent('App1', () => App);
Navigation.registerComponent('App2', () => App2);
Navigation.registerComponent('SideMenu', () => SideMenu);
Navigation.registerComponent('SayakoMainMenu', () => SayakoMainMenu);
Navigation.registerComponent('RegisterAndLogin', () => RegisterAndLogin);
Navigation.registerComponent('HomePage', () => HomePage);
Navigation.registerComponent('OrderPage', () => OrderPage);
Navigation.registerComponent('EditPage', () => EditPage);
Navigation.registerComponent('AddItem', () => AddItem);
Navigation.registerComponent('ProfilePage', () => ProfilePage);
Navigation.registerComponent('LoginPage', () => LoginPage);
Navigation.registerComponent('RegisterPage', () => RegisterPage);
Navigation.registerComponent('BenefitPage', () => BenefitPage);
Navigation.registerComponent('TransferHistoryPage', () => TransferHistoryPage);
Navigation.registerComponent('HistoryPage', () => HistoryPage);

Navigation.events().registerAppLaunchedListener(() => {
    Navigation.setDefaultOptions({
        topBar: {
            visible: false,
            drawBehind: true,
            animate: false,
        }
    });
    Navigation.setRoot({
        root: {
            stack: {
                children: [{
                    component: {
                        name: 'LoginPage',
                    }
                }],
                options: {}
            }



        }
    });
});

