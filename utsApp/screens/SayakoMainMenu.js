import React, {Component} from 'react';
import {Button, Platform, StyleSheet, Text, View, TouchableOpacity, Image, Dimensions, ScrollView, ActivityIndicator} from 'react-native';
import {Navigation} from "react-native-navigation";

var {height, width} = Dimensions.get('window');

export default class SayakoMainMenu extends Component<Props> {







  onPress = () => {
      console.log('push to App 2')
      // Navigation.push(this.props.componentId, {
      //     component: {
      //         name: 'App2',
      //
      //     }
      // });

      Navigation.showOverlay({
          component: {
              name: 'SayakoMainMenu',
              // options: {
              //     overlay: {
              //         interceptTouchOutside: true
              //     }
              // }
          }
      });

  }

    hideSideMenu = () => {
        Navigation.mergeOptions(this.props.componentId, {
            sideMenu: {
                left: {
                    visible: true
                }
            }
        });
    }

    constructor(props) {
        super(props);
        Navigation.events().bindComponent(this);
    }




    render() {
    return (
      <View style={styles.container}>
          <View style={styles.topMenu}>
              <TouchableOpacity onPress={this.hideSideMenu}>
                  <Image style={{margin: 10}} source={require('../assets/img/hamburgerMenu.png')} />
              </TouchableOpacity>
              <Text style={styles.topMenuText}>Popular Recipes</Text>
              <Image style={{margin: 10}} source={require('../assets/img/search.png')} />
          </View>
        <View style={styles.bigMenu}>
            <TouchableOpacity onPress={this.onPress}>
                <Text style={styles.bigMenuText}>Nyaneut</Text>
            </TouchableOpacity>
            <TouchableOpacity onPress={this.onPress}>
                <Text style={styles.bigMenuText}>Tuang</Text>
            </TouchableOpacity>
            <TouchableOpacity onPress={this.onPress}>
                <Text style={styles.bigMenuText}>Nginum</Text>
            </TouchableOpacity>
        </View>
          <View style={{height: 200}}>
              <ScrollView showsHorizontalScrollIndicator={false}
                           horizontal contentContainerStyle={styles.foodMenu}>
                  <Image style={{height: 150, width: 150,}} source={require('../assets/img/rendang.png')} />
                  <Image style={{height: 200, width: 200,}} source={require('../assets/img/nasiGoreng.png')} />
                  <Image style={{height: 150, width: 150,}} source={require('../assets/img/sate.png')} />
              </ScrollView>
          </View>
          <View style={styles.rating}>
              <Image source={require('../assets/img/rating4.png')} />
              <Text style={styles.foodTitle}>
                  Sangu Jelek
              </Text>
              <View style={styles.foodDescriptionContainer}>
                  <Text style={styles.foodDescription}>
                      It can refer simply to fried pre-cooked rice,
                      a meal including stir fried rice in a small amount
                      of cooking oil or margarine, typically spiced with
                      kecap manis (sweet soy sauce), shallot, garlic, ground
                      shrimp paste, tamarind and chilli and accompanied by other
                      ingredients, particularly egg, chicken and prawns.
                  </Text>
              </View>

          </View>
          <View>

          </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#F5FCFF',
  },
    topMenu: {
        flexDirection: 'row',
        justifyContent: 'space-around',
        marginTop: 50,
    },
  topMenuText: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
    bigMenu: {
        flexDirection: 'row',
        justifyContent: 'space-around',

    },
    bigMenuText: {
        color: '#BB2424',
        padding: 20
    },
    foodMenu: {
      alignItems: 'center',
    },
    rating: {
      padding: 30,
      alignItems: 'center'

    },
    foodTitle: {
      fontSize: 20,
        padding: 20,
    },
    foodDescriptionContainer: {
      width: width-100,


    },
    foodDescription: {
        textAlign: 'justify',
    },
});
