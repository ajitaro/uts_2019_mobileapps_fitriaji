import React, {Component} from 'react';
import {Button, Platform, StyleSheet, Text, View,
    TouchableOpacity, Image, Dimensions, ScrollView,
    ActivityIndicator, ImageBackground} from 'react-native';
import {Navigation} from "react-native-navigation";

var {height, width} = Dimensions.get('window');

export default class RegisterAndLogin extends Component<Props> {
    constructor(props) {
        super(props);
        Navigation.events().bindComponent(this);
    }
    onPress = () => {

    }




    render() {
    return (
        <ImageBackground source={require('../assets/img/prasmulPay_Background.png')} style={styles.backgroundImage}>
            <View style={styles.whiteOverlay}>
                <Image source={require('../assets/img/LogoPrasmulpay.png')} style={styles.logo}/>
                <TouchableOpacity
                    style={styles.loginButton}
                    onPress={this.onPress}>
                    <Text style={{color: '#FFFFFF'}}>LOG IN</Text>
                </TouchableOpacity>
                <TouchableOpacity
                    style={styles.createButton}
                    onPress={this.onPress}>
                    <Text style={{color: '#007EA7'}}>CREATE</Text>
                </TouchableOpacity>
            </View>
        </ImageBackground>

    );
  }
}

const styles = StyleSheet.create({
    backgroundImage: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
    },
    whiteOverlay: {
        width: '100%',
        height: '100%',
        backgroundColor: 'rgba(255, 255, 255, .5)',
        alignItems: 'center',
        justifyContent: 'center',


    },
    loginButton: {
        alignItems: 'center',
        backgroundColor: '#007EA7',
        paddingVertical: 10,
        paddingHorizontal: 130,
        borderRadius: 20,
        margin: 10,
    },
    createButton: {
        alignItems: 'center',
        backgroundColor: '#FFFFFF',
        paddingVertical: 10,
        paddingHorizontal: 130,
        borderRadius: 20,
    }

});
