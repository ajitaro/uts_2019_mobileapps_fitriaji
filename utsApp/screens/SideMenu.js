/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, {Component} from 'react';
import {Platform, StyleSheet, Text, View, TouchableOpacity, Image, Dimensions} from 'react-native';
import {Navigation} from "react-native-navigation";

var {height, width} = Dimensions.get('window');

export default class SideMenu extends Component<Props> {



    onPressPopularRecipes = () => {
        console.log('push to App 2')
        // Navigation.push(this.props.componentId, {
        //     component: {
        //         name: 'App2',
        //
        //     }
        // });

        Navigation.showOverlay({
            component: {
                name: 'SayakoMainMenu',
                // options: {
                //     overlay: {
                //         interceptTouchOutside: true
                //     }
                // }
            }
        });

    }

    render() {
        return (
            <View style={styles.container}>
                <View style={styles.menu}>
                    <TouchableOpacity onPress={this.onPressPopularRecipes}>
                        <Text style={styles.menuText}>Popular Recipes</Text>
                    </TouchableOpacity>
                    <TouchableOpacity onPress={this.onPress}>
                        <Text style={styles.menuText}>Saved Recipes</Text>
                    </TouchableOpacity>
                    <TouchableOpacity onPress={this.onPress}>
                        <Text style={styles.menuText}>Shopping List</Text>
                    </TouchableOpacity>
                    <TouchableOpacity onPress={this.onPress}>
                        <Text style={styles.menuText}>Settings</Text>
                    </TouchableOpacity>
                </View>

                <View>
                    <Image style={styles.profilePicture} source={require('../assets/img/profilePic.png')} />
                    <Text style={styles.profileText}>Enakorin Sakurata</Text>
                </View>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'flex-start',
        alignItems: 'flex-start',
        backgroundColor: '#BB2424',


    },
    menu: {
        flex: 1,
        textAlign: 'left',
        marginStart: 30,
        marginTop: 60,
    },
    menuText: {
        fontSize: 20,
        color: 'white',
        marginBottom: 20,
    },
    profilePicture: {
        width: 70,
        height: 70,
        marginStart: 30,
        marginBottom: 20,
    },
    profileText: {
        fontSize: 16,
        color: 'white',
        marginStart: 30,
        marginBottom: 60,
    },
});
