import React, {Component} from 'react';
import {Button, Platform, StyleSheet, Text, View,
    TouchableOpacity, Image, Dimensions, ScrollView,
    ActivityIndicator, ImageBackground, TextInput} from 'react-native';
import {Navigation} from "react-native-navigation";

var {height, width} = Dimensions.get('window');

export default class RegisterPage extends Component {

    _buttonPressed  = (screenName) => {
        Navigation.push(this.props.componentId, {
            component: {
                name: screenName
            }
        });

    };
    _createButtonPressed = () => {
        Navigation.setRoot({
            root: {
                bottomTabs: {
                    children: [
                        {
                            stack: {
                                children: [{
                                    component: {
                                        name: 'HomePage'
                                    },
                                }],
                                options: {
                                    bottomTab: {
                                        text: 'Home',
                                        icon: require('../../assets/icons/homeIcon.png')
                                    }
                                }

                            }
                        },
                        {
                            stack: {
                                children: [{
                                    component: {
                                        name: 'OrderPage'
                                    },
                                }],
                                options: {
                                    bottomTab: {
                                        text: 'Order',
                                        icon: require('../../assets/icons/orderIcon.png')
                                    }
                                }
                            }
                        },
                        {
                            stack: {
                                children: [{
                                    component: {
                                        name: 'EditPage'
                                    },
                                }],
                                options: {
                                    bottomTab: {
                                        text: 'Edit',
                                        icon: require('../../assets/icons/editIcon.png')
                                    }
                                }
                            }
                        },
                        {
                            stack: {
                                children: [{
                                    component: {
                                        name: 'ProfilePage'
                                    },
                                }],
                                options: {
                                    bottomTab: {
                                        text: 'Profile',
                                        icon: require('../../assets/icons/profileIcon.png')
                                    }
                                }
                            }
                        },
                    ],
                    options: {}
                }
            }
        });
    };

    render() {
        return (
            <ImageBackground source={require('../../assets/img/prasmulPay_Background.png')} style={styles.backgroundImage}>
                <View style={styles.whiteOverlay}>
                    <Image source={require('../../assets/img/prasmulWelcome.png')} style={styles.logo}/>

                    {/*Kotak Putih*/}

                    <View style={styles.loginInput}>
                        <TextInput
                            style={styles.userInput}
                            onChangeText={(text) => this.setState({text})}
                            placeholder={'New Username'}
                            placeholderTextColor={'#007EA7'}
                        />
                        <Image source={require('../../assets/icons/accountIcon.png')} style={styles.loginIcon}/>
                    </View>

                    {/*Kotak Putih*/}

                    <View style={styles.loginInput}>
                        <TextInput
                            style={styles.userInput}
                            onChangeText={(text) => this.setState({text})}
                            placeholder={'New Email'}
                            placeholderTextColor={'#007EA7'}
                        />
                        <Image source={require('../../assets/icons/email.png')} style={styles.loginIcon}/>
                    </View>

                    {/*Kotak Putih*/}

                    <View style={styles.loginInput}>
                        <TextInput
                            style={styles.userInput}
                            onChangeText={(text) => this.setState({text})}
                            placeholder={'New Password'}
                            secureTextEntry={true}
                            placeholderTextColor={'#007EA7'}
                        />
                        <Image source={require('../../assets/icons/lock.png')} style={styles.loginIcon}/>
                    </View>

                    {/*Kotak Putih*/}

                    <View style={styles.loginInput}>
                        <TextInput
                            style={styles.userInput}
                            onChangeText={(text) => this.setState({text})}
                            placeholder={'Confirm Password'}
                            secureTextEntry={true}
                            placeholderTextColor={'#007EA7'}
                        />
                        <Image source={require('../../assets/icons/lock.png')} style={styles.loginIcon}/>
                    </View>


                    <TouchableOpacity
                        style={styles.loginButton}
                        onPress={this._createButtonPressed}
                    >
                        <Text style={{color: '#FFFFFF'}}>
                            CREATE
                        </Text>
                    </TouchableOpacity>

                    <View style={styles.dontHave}>
                        <Text style={styles.loginText}>
                            Already have any account?
                        </Text>
                        <TouchableOpacity
                            onPress={ () =>    Navigation.pop(this.props.componentId)}
                        >
                            <Text style={styles.createAccount}>
                                Log In
                            </Text>
                        </TouchableOpacity>
                    </View>

                </View>
            </ImageBackground>
        )
    }
}

const styles = StyleSheet.create({
    backgroundImage: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
    },
    whiteOverlay: {
        width: '100%',
        height: '100%',
        backgroundColor: 'rgba(255, 255, 255, .5)',
        alignItems: 'center',
        justifyContent: 'center',


    },
    logo: {
        marginBottom: 80
    },
    loginButton: {
        alignItems: 'center',
        backgroundColor: '#007EA7',
        paddingVertical: 10,
        paddingHorizontal: 130,
        borderRadius: 20,
        marginVertical: 30,
    },
    createButton: {
        alignItems: 'center',
        backgroundColor: '#FFFFFF',
        paddingVertical: 10,
        paddingHorizontal: 130,
        borderRadius: 20,
    },
    loginInput: {
        flexDirection: 'row',
        height: 55,
        width: 330,
        alignItems: 'center',
        justifyContent: 'space-between',
        backgroundColor: "#FFFFFF",
        borderRadius: 10,
        margin: 10
    },
    loginIcon: {
        marginRight: 20
    },
    userInput: {
        paddingLeft: 20
    },
    dontHave: {
        flexDirection: 'row',
        alignItems: 'center',
        marginTop: 10,
    },
    loginText: {
        fontSize: 18,
        color: '#FFFFFF',
        textShadowColor: '#00000040',
        textShadowRadius: 5,
        textShadowOffset: {width: 4, height: 3}
    },
    createAccount: {
        color: '#00D1FF',
        fontWeight: 'bold',
        fontSize: 18,
        textShadowColor: '#00000040',
        textShadowRadius: 5,
        margin: 5,
        textShadowOffset: {width: 4, height: 3}
    },

});
