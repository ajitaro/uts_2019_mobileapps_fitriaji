import React, {Component} from 'react';
import {
    Button, Platform, StyleSheet, Text, View,
    TouchableOpacity, Image, Dimensions, ScrollView,
    ActivityIndicator, ImageBackground, TextInput, Alert, FlatList
} from 'react-native';

import {Navigation} from "react-native-navigation/lib/dist/index";
import Modal from 'react-native-modalbox';
import flatListHistoryData from '../../Data/flatListHistoryData';
import flatListFoodData from "../../Data/flatListFoodData";


var {height, width} = Dimensions.get('window');

class FlatListItem extends Component {
    constructor(props) {
        super(props);
        this.state = {
            activeRowKey: null
        };

    }

    render() {
        return (
            <View style={styles.boxPembeli}>
                <View>
                    <Image style={styles.whiteBoxPic} source={{uri: this.props.item.image}}/>
                </View>

                <View style={styles.detailContainer}>
                    <Text style={styles.historyName}>
                        {this.props.item.name}
                    </Text>
                    <Text style={styles.historyDate}>
                        {this.props.item.date}
                    </Text>
                    <Text style={styles.historyNim}>
                        {this.props.item.key}
                    </Text>
                </View>


                <View style={styles.viewStatus}>
                    <Text style={styles.textStatus}>
                        {this.props.item.status}
                    </Text>
                </View>
                <View style={styles.titikTiga}>
                    <Image source={require('../../assets/icons/threePointsIcon.png')} />
                </View>

            </View>
        )
    }
}

export default class HistoryPage extends Component {
    constructor(props) {
        super(props);
        this.state = ({
            deltedRowKey: null,
        });
    };
    _onPressed = () => {
        Navigation.pop(this.props.componentId)
    };



    render() {
    return (
        <ScrollView
            contentContainerStyle={{
                justifyContent: 'center',

            }}

        >
            <View style={styles.background}>
                <View style={styles.container}>
                    <View style={styles.tobBar}>
                        <TouchableOpacity
                            onPress={ () =>    Navigation.pop(this.props.componentId)}
                        >
                            <Image source={require('../../assets/icons/cancelIcon.png')}/>
                        </TouchableOpacity>
                        <Text style={styles.tobBarText}>
                            History
                        </Text>
                    </View>

                    {/*White Box*/}

                    <View style={styles.whiteBoxBot}>

                        <FlatList
                            ref={'flatList'}
                            data={flatListHistoryData}
                            renderItem={({item, index}) => {
                                // console.log(`Item = ${JSON.stringify(item)}, index = ${index}`);
                                return(
                                    <FlatListItem item={item} index={index} parentFlatList={this}>
                                    </FlatListItem>
                                );
                            }}
                        >

                        </FlatList>



                    </View>
                </View>
            </View>
        </ScrollView>


    );
  }
}

const styles = StyleSheet.create({
    background: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
    },
    container: {
        width: '100%',
        height: '100%',
        alignItems: 'center',


    },
    tobBar: {
        width: width,
        backgroundColor: '#007EA7',
        padding: 10,
        justifyContent: 'center',

    },
    tobBarText: {
        color: '#FFFFFF',
        fontSize: 20,
        fontWeight: 'bold',
        textAlign: 'center'
    },
    defaultMealPic: {
        width: width-20,
        height: 200,
        backgroundColor: '#C4C4C4',
        justifyContent: 'center',
        alignItems: 'center',
        marginVertical: 20,
    },
    bottomLine: {
        width: width-40,
        borderBottomColor: '#C4C4C4',
        borderBottomWidth: 1,
        marginVertical: 10,
    },
    addButton: {
        width: 105,
        height: 35,
        backgroundColor: '#007EA7',
        borderRadius: 30,
        justifyContent: 'center',
        alignItems: 'center',
        alignSelf: 'flex-end',
        margin: 30

    },
    addButtonText:{
        fontSize: 21,
        fontWeight: 'bold',
        color: '#FFFFFF',

    },
    whiteBox: {
        flexDirection: 'row',
        backgroundColor: '#FFFFFF',
        width: width,
        padding: 15,
        alignItems: 'center',
        justifyContent: 'space-between',
        marginBottom: 40
    },
    whiteBoxTitle: {
        fontSize: 18,
        paddingBottom: 5
    },
    whiteBoxDesc: {
        fontSize: 13,
        width: 186
    },
    whiteBoxButton: {
        backgroundColor: '#007EA7',
        borderRadius: 20,
        height: 40,
        width: 64,
        justifyContent: 'center',
        alignItems: 'center'
    },
    whiteBoxButtonText: {
        color: '#FFFFFF',
        fontSize: 18
    },
    whiteBoxBot: {
        backgroundColor: '#FFFFFF',
        width: width,
        padding: 15,
        marginBottom: 40
    },
    boxPembeli: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        paddingBottom: 20
    },
    whiteBoxPic: {
        height: 60,
        width: 60,
        borderRadius: 60/2,
    },
    detailContainer: {
        width: 180,
        paddingLeft: 10,
    },
    historyName: {
        fontWeight: 'bold',
        fontSize: 16,
        color: '#404040'
    },
    historyDate: {
        fontSize: 13,
        color: '#C4C4C4',
    },
    historyNim: {
        fontSize: 13,
        fontWeight: 'bold',
        color: '#767676'
    },

    viewStatus: {
        width: 70,
        justifyContent: 'center',
        alignItems: 'center',
    },
    textStatus: {
        color: '#FEC427',
        fontSize: 13,
        fontWeight: 'bold',
    },
    titikTiga: {
        width: 7,
        justifyContent: 'center',
        alignItems: 'center',
        marginRight: 20,
    }

});
