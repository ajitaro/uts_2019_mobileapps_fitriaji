import React, {Component} from 'react';
import {Button, Platform, StyleSheet, Text, View,
    TouchableOpacity, Image, Dimensions, ScrollView,
    ActivityIndicator, ImageBackground} from 'react-native';
import {Navigation} from "react-native-navigation/lib/dist/index";
import BenefitPage from "./BenefitPage";
import NotificationPopUp from "./NotificationPopUp";



var {height, width} = Dimensions.get('window');

export default class HomePage extends Component {
    constructor(props) {
        super(props);
        Navigation.events().bindComponent(this);
        this._onPressNotif = this._onPressNotif.bind(this);

    }
    _onPressNotif () {
        this.refs.popNotif.showNotification();
    }
    _onPressed = (screenName) => {
        Navigation.push(this.props.componentId, {
            component: {
                name: screenName
            }
        });

    };




    render() {
    return (
        <ImageBackground source={require('../../assets/img/prasmulPay_Background.png')} style={styles.backgroundImage}>
            <View style={styles.whiteOverlay}>

                    {/*
                ======>
                Kotak Putih
                ======>
                */}

                    <View style={styles.whiteBox}>
                        <View style={styles.whiteBoxText}>
                            <Text style={{
                                fontSize: 24,
                                color: '#404040'
                            }}>
                                Hi. Sayako
                            </Text>
                            <Text style={{
                                fontSize: 16,
                                color: '#404040'
                            }}>
                                ID: 02350171004
                            </Text>
                        </View>
                        <View style={styles.whiteBoxIcon}>
                            <TouchableOpacity
                                onPress={ () => this._onPressed ('HistoryPage')}
                            >
                                <Image style={{
                                    marginHorizontal: 10,
                                }} source={require('../../assets/icons/historyIcon.png')}/>
                            </TouchableOpacity>
                            <TouchableOpacity
                                onPress={this._onPressNotif}
                            >
                                <Image style={{
                                    marginHorizontal: 10,
                                }} source={require('../../assets/icons/notificationIcon.png')}/>
                            </TouchableOpacity>
                        </View>
                    </View>

                    {/*
                ======>
                Today's Benefit
                ======>
                */}

                    <TouchableOpacity
                        onPress={ () => this._onPressed ('BenefitPage')}
                        style={styles.todayBenefit}
                    >
                        <Text style={{
                            color: '#E5E5E5',
                            fontSize: 20,
                            fontWeight: 'bold'
                        }}>
                            Today's Benefit
                        </Text>
                        <Text style={{
                            color: '#FFFFFF',
                            fontSize: 30,
                        }}>
                            Rp. 1.420.000,-
                        </Text>
                        <Text style={{
                            color: '#FFFFFF',
                            fontSize: 10,
                        }}>
                            Last Updated at 14.00
                        </Text>
                    </TouchableOpacity>

                    {/*
                ======>
                New Order
                ======>
                */}

                    <View style={styles.boxTitle}>
                        <Text style={styles.boxTitleText}>New Order</Text>
                        <Text style={styles.seeMore}>See More</Text>
                    </View>

                <ScrollView
                    horizontal={true}
                    showsHorizontalScrollIndicator={false}
                >
                <View style={styles.blueBox}>
                        <View style={styles.blueBoxPicAndText}>
                            <Image source={require('../../assets/img/sayako.jpg')}
                                   style={styles.blueBoxPic}/>
                            <View style={styles.blueBoxText}>
                                <Text style={styles.blueBoxName}>Baihaqi Saputra</Text>
                                <View style={styles.blueBoxDesc}>
                                    <View>
                                        <Text style={styles.blueBoxDescText}>1 pcs</Text>
                                        <Text style={styles.blueBoxDescText}>1 pcs</Text>
                                        <Text style={styles.blueBoxDescText}>2 pcs</Text>
                                    </View>
                                    <View>
                                        <Text style={styles.blueBoxDescText}>Ayam Waito</Text>
                                        <Text style={styles.blueBoxDescText}>Nasi Merah</Text>
                                        <Text style={styles.blueBoxDescText}>Teh Sisir Gila Batu</Text>
                                    </View>
                                </View>
                            </View>
                        </View>
                        <Text style={styles.blueBoxButton}>ACCEPT</Text>
                    </View>

                    <View style={styles.blueBox}>
                        <View style={styles.blueBoxPicAndText}>
                            <Image source={require('../../assets/img/sayako.jpg')}
                                   style={styles.blueBoxPic}/>
                            <View style={styles.blueBoxText}>
                                <Text style={styles.blueBoxName}>Baihaqi Saputra</Text>
                                <View style={styles.blueBoxDesc}>
                                    <View>
                                        <Text style={styles.blueBoxDescText}>1 pcs</Text>
                                        <Text style={styles.blueBoxDescText}>1 pcs</Text>
                                        <Text style={styles.blueBoxDescText}>2 pcs</Text>
                                    </View>
                                    <View>
                                        <Text style={styles.blueBoxDescText}>Ayam Waito</Text>
                                        <Text style={styles.blueBoxDescText}>Nasi Merah</Text>
                                        <Text style={styles.blueBoxDescText}>Teh Sisir Gila Batu</Text>
                                    </View>
                                </View>
                            </View>
                        </View>
                        <Text style={styles.blueBoxButton}>ACCEPT</Text>
                    </View>
                </ScrollView>

                    {/*
                ======>
                On Progress
                ======>
                */}

                    <View style={styles.boxTitle}>
                        <Text style={styles.boxTitleText}>On Progress</Text>
                        <Text style={styles.seeMore}>See More</Text>
                    </View>

                <ScrollView
                    horizontal={true}
                    showsHorizontalScrollIndicator={false}
                >
                <View style={styles.blueBox}>
                        <View style={styles.blueBoxPicAndText}>
                            <Image source={require('../../assets/img/sayako.jpg')}
                                   style={styles.blueBoxPic}/>
                            <View style={styles.blueBoxText}>
                                <Text style={styles.blueBoxName}>Putu Krisna</Text>
                                <View style={styles.blueBoxDesc}>
                                    <View>
                                        <Text style={styles.blueBoxDescText}>1 pcs</Text>
                                        <Text style={styles.blueBoxDescText}>1 pcs</Text>
                                        <Text style={styles.blueBoxDescText}>3 pcs</Text>
                                    </View>
                                    <View>
                                        <Text style={styles.blueBoxDescText}>Ayam Blekping</Text>
                                        <Text style={styles.blueBoxDescText}>Nasi Goreng (Tanpa telur)</Text>
                                        <Text style={styles.blueBoxDescText}>Susu Kenyal Manis</Text>
                                    </View>
                                </View>
                            </View>
                        </View>
                        <Text style={styles.blueBoxButton}>FINISH</Text>
                    </View>

                    <View style={styles.blueBox}>
                        <View style={styles.blueBoxPicAndText}>
                            <Image source={require('../../assets/img/sayako.jpg')}
                                   style={styles.blueBoxPic}/>
                            <View style={styles.blueBoxText}>
                                <Text style={styles.blueBoxName}>Putu Krisna</Text>
                                <View style={styles.blueBoxDesc}>
                                    <View>
                                        <Text style={styles.blueBoxDescText}>1 pcs</Text>
                                        <Text style={styles.blueBoxDescText}>1 pcs</Text>
                                        <Text style={styles.blueBoxDescText}>3 pcs</Text>
                                    </View>
                                    <View>
                                        <Text style={styles.blueBoxDescText}>Ayam Blekping</Text>
                                        <Text style={styles.blueBoxDescText}>Nasi Goreng (Tanpa telur)</Text>
                                        <Text style={styles.blueBoxDescText}>Susu Kenyal Manis</Text>
                                    </View>
                                </View>
                            </View>
                        </View>
                        <Text style={styles.blueBoxButton}>FINISH</Text>
                    </View>
                </ScrollView>

                {/*
                ======>
                Copyright
                ======>*/}

                <View style={{flex: 1, justifyContent: 'flex-end'}}>
                    <View style={styles.copyright}>
                        <Text style={[styles.copyrightText, {fontSize: 9, fontWeight: 'bold'}]}>S1 Software Engineering</Text>
                        <Text style={[styles.copyrightText, {fontSize: 12.5, fontWeight: 'bold'}]}>Universitas Prasetiya Mulya</Text>
                        <Text style={[styles.copyrightText, {fontSize: 9}]}>Copyright 2018</Text>
                    </View>
                </View>

            </View>
            <NotificationPopUp ref={'popNotif'}/>
        </ImageBackground>

    );
  }
}

const styles = StyleSheet.create({
    backgroundImage: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
    },
    whiteOverlay: {
        flex: 1,
        width: '100%',
        height: '100%',
        backgroundColor: 'rgba(255, 255, 255, .5)',
        alignItems: 'center'


    },
    whiteBox: {
        flexDirection: 'row',
        backgroundColor: '#FFFFFF',
        width: width,
        paddingVertical: 5,
        marginTop: 10,
    },
    whiteBoxIcon: {
        flex: 1,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'flex-end',
        paddingRight: 10

    },
    whiteBoxText: {
        flex: 1,
        paddingLeft: 10,
    },
    todayBenefit: {
        height: 110,
        width: width-30,
        borderRadius: 25,
        backgroundColor: '#003459',
        marginTop: 10,
        marginBottom: 30,
        justifyContent: 'center',
        paddingHorizontal: 20,
    },
    boxTitle: {
        flexDirection: 'row',
        width: width-50,
        alignItems: 'center',
        margin: 10


    },
    boxTitleText: {
        flex: 1,
        fontSize: 24,
        fontWeight: 'bold',
        color: '#FFFFFF',
    },
    seeMore: {
        flex: 1,
        fontSize: 14,
        fontWeight: 'bold',
        color: '#00171F',
        textAlign: 'right'

    },
    blueBox: {
        height: 140,
        width: 340,
        borderRadius: 50,
        backgroundColor: '#003459',
        justifyContent: 'center',
        paddingHorizontal: 20,
        marginHorizontal: 20
    },
    blueBoxPic: {
        height: 50,
        width: 50,
        borderRadius: 50/2
    },
    blueBoxDesc: {
        flexDirection: 'row',
    },
    blueBoxPicAndText: {
        flexDirection: 'row',
        paddingBottom: 10
    },
    blueBoxName: {
        color: '#FFFFFF',
        fontSize: 16,
        padding: 5
    },
    blueBoxDescText: {
        color: '#FFFFFF',
        fontSize: 12,
        paddingHorizontal: 15
    },
    blueBoxButton: {
        color: '#FFFFFF',
        fontSize: 12,
        fontWeight: 'bold',
        textAlign: 'right',
        paddingHorizontal: 30
    },
    copyright: {
        width: width,
        backgroundColor: '#E9E9E9BF',
        alignItems: 'center',
        justifyContent: 'center',
        padding: 10

    },
    copyrightText: {
        color: '#646464'

    },

});
