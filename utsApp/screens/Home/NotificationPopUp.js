import React, {Component} from 'react';
import {Button, Platform, StyleSheet, Text, View,
    TouchableOpacity, Image, Dimensions, ScrollView,
    ActivityIndicator, ImageBackground, TextInput} from 'react-native';

import {Navigation} from "react-native-navigation/lib/dist/index";
import Modal from 'react-native-modalbox';

var {height, width} = Dimensions.get('window');

export default class NotificationPopUp extends Component {

    showNotification = () => {
        this.refs.notification.open();
    };

    render() {
        return (
            <Modal
                ref={"notification"}
                style={styles.popUp}
                position='center'
                backdrop={true}
                onClosed={() => {
                    // alert("Modal closed");
                }}
            >
                <View style={styles.tobBar}>
                    <TouchableOpacity
                        style={styles.cancelButton}
                        onPress={() => {
                            this.refs.notification.close()}}
                    >
                        <Image source={require('../../assets/icons/blackCancelIcon.png')}/>
                    </TouchableOpacity>
                    <View style={styles.notificationTitle}>
                        <Image source={require('../../assets/icons/notificationIcon.png')}/>
                        <Text style={styles.tobBarText}>
                            Notification
                        </Text>
                    </View>
                </View>

                <View styles={styles.newView}>
                    <Text style={styles.newMessage}>
                        New
                    </Text>
                    <Text style={styles.messageText}>
                        There are 2 order on your list. Go check your Order List!
                    </Text>
                </View>

                <View styles={styles.newView}>
                    <Text style={styles.newMessage}>
                        New
                    </Text>
                    <Text style={styles.messageText}>
                        Your benefit is passing Rp. 1.000.000,- today. Congratulations!!!
                    </Text>
                </View>
            </Modal>
        )
    }
}

const styles = StyleSheet.create({
    popUp:{
        width: 330,
        height: 420,
        alignItems: 'center',
        borderRadius: 10
    },
    tobBar: {
        margin: 20,
        width: 290,
    },
    cancelButton: {

    },
    notificationTitle: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center'
    },
    newView: {

    },
    newMessage: {
        color: '#00D1FF',
        fontSize: 14,
        fontWeight: 'bold',
        width: 290,
        marginBottom: 10,
    },
    messageText: {
        fontSize: 14,
        color: '#404040',
        width: 290,
        marginBottom: 30,
    },


});