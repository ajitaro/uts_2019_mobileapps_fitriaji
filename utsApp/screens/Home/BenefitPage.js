import React, {Component} from 'react';
import {
    Button, Platform, StyleSheet, Text, View,
    TouchableOpacity, Image, Dimensions, ScrollView,
    ActivityIndicator, ImageBackground, TextInput, Alert, FlatList
} from 'react-native';

import {Navigation} from "react-native-navigation"



var {height, width} = Dimensions.get('window');

export default class BenefitPage extends Component {
    _onPressed = (screenName) => {
        Navigation.push(this.props.componentId, {
            component: {
                name: screenName,
                id: screenName
            }
        });

    };

    constructor(props){
        super(props);

    }
    render() {
        return (
            <View style={styles.container}>

                {/*
                ======>
                Today's Benefit
                ======>
                */}

                <View style={styles.todayBenefit}>
                    <Text style={{
                        color: '#E5E5E5',
                        fontSize: 20,
                        fontWeight: 'bold'
                    }}>
                        Today's Benefit
                    </Text>
                    <Text style={{
                        color: '#FFFFFF',
                        fontSize: 30,
                    }}>
                        Rp. 1.420.000,-
                    </Text>
                    <Text style={{
                        color: '#FFFFFF',
                        fontSize: 10,
                    }}>
                        Last Updated at 14.00
                    </Text>
                </View>

                {/*
                =====>
                Calendar
                =====>
                */}

                <Image source={require('../../assets/img/calendarPic.png')}/>

                {/*
                =====>
                Week and Month Benefit
                =====>
                */}

                <View>
                    <View style={styles.thisWeekView}>
                        <Text style={styles.thisWeekWord}>
                            This week's Benefit :
                        </Text>
                        <Text style={styles.thisWeekNumber}>
                            Rp. 10.520.000,-
                        </Text>
                        <Text style={styles.thisWeekWord}>
                            This month's Benefit :
                        </Text>
                        <Text style={styles.thisWeekNumber}>
                            Rp. 15.760.000,-
                        </Text>
                    </View>

                    <TouchableOpacity
                        onPress={() => this._onPressed('TransferHistoryPage')
                        }
                        style={styles.transferHistoryButton}
                    >

                        {/*
                        =====>
                        Check Transfer History
                        =====>
                        */}

                        <Text style={styles.transferHistoryButtonText}>
                            Check Transfer History
                        </Text>
                    </TouchableOpacity>
                </View>

            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
    },
    todayBenefit: {
        height: 110,
        width: width-30,
        borderRadius: 25,
        backgroundColor: '#003459',
        marginTop: 10,
        marginBottom: 40,
        justifyContent: 'center',
        paddingHorizontal: 20,
    },
    thisWeekView:{
        justifyContent: 'center',
        margin: 10
    },
    thisWeekWord: {
        color: '#003459',
        fontWeight: 'bold',
        fontSize: 18,
        padding: 5,
    },
    thisWeekNumber: {
        fontWeight: 'bold',
        fontSize: 24,
        textAlign: 'center',
        padding: 5,
    },
    transferHistoryButton: {
        height: 36,
        width: 300,
        backgroundColor: '#007EA7',
        borderRadius: height/2,
        alignItems: 'center',
        justifyContent: 'center',
    },
    transferHistoryButtonText: {
        color: '#FFFFFF',
        fontSize: 21,
        fontWeight: 'bold'
    },
});

