import React, {Component} from 'react';
import {
    Button, Platform, StyleSheet, Text, View,
    TouchableOpacity, Image, Dimensions, ScrollView,
    ActivityIndicator, ImageBackground, TextInput, Alert, FlatList
} from 'react-native';

import {Navigation} from "react-native-navigation"
import transferHistoryData from "../../../Data/transferHistoryData";
import TransferHistoryPopUp from "./TransferHistoryPopUp";



var {height, width} = Dimensions.get('window');


class FlatListItem extends Component {
    render() {
        return (
            <View>
                <View
                    style={styles.boxPembeli}>
                    <View>
                        <Image style={styles.whiteBoxPic} source={{uri: this.props.item.image}}/>
                    </View>

                    <View style={styles.detailContainer}>
                        <Text style={styles.historyName}>
                            {this.props.item.name}
                        </Text>
                        <Text style={styles.historyDate}>
                            {this.props.item.date}
                        </Text>
                        <Text style={styles.historyNim}>
                            {this.props.item.note}
                        </Text>
                    </View>


                    <View style={styles.viewStatus}>
                        <Text style={styles.textStatus}>
                            {this.props.item.status}
                        </Text>
                    </View>
                    <View style={styles.titikTiga}>
                        <Image source={require('../../../assets/icons/threePointsIcon.png')} />
                    </View>

                </View>

            </View>
        )
    }
}

export default class TransferHistoryPage extends Component {
    constructor(props) {
        super(props);
        this._onPressWhiteBox = this._onPressWhiteBox.bind(this);
    }
    _onPressWhiteBox () {
        this.refs.popHistory.showHistory();
    }
    _onPressed = () => {
        Navigation.pop(this.props.componentId)
    };



    render() {
        return (
            <View style={styles.container}>
                <View style={styles.tobBar}>
                    <TouchableOpacity
                        onPress={ () => Navigation.pop(this.props.componentId)}
                    >
                        <Image source={require('../../../assets/icons/cancelIcon.png')}/>
                    </TouchableOpacity>
                    <Text style={styles.tobBarText}>
                        Transfer History
                    </Text>
                </View>

                {/*White Box*/}

                <View
                    style={styles.whiteBoxBot}
                >

                    <FlatList
                        ref={'flatList'}
                        data={transferHistoryData}
                        keyExtractor={(item) => item.id}
                        renderItem={({item, index}) => (
                            <TouchableOpacity
                                onPress={ () => this._onPressWhiteBox(item)}
                            >

                                    <FlatListItem item={item} index={index} parentFlatList={this}/>

                            </TouchableOpacity>

                        )}
                    >

                    </FlatList>



                </View>
                <TransferHistoryPopUp ref={'popHistory'}/>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    background: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
    },
    container: {
        width: '100%',
        height: '100%',
        alignItems: 'center',


    },
    tobBar: {
        width: width,
        backgroundColor: '#007EA7',
        padding: 10,
        justifyContent: 'center',

    },
    tobBarText: {
        color: '#FFFFFF',
        fontSize: 20,
        fontWeight: 'bold',
        textAlign: 'center'
    },
    whiteBoxBot: {
        backgroundColor: '#FFFFFF',
        width: width,
        padding: 15,
        marginBottom: 40
    },
    whiteBoxPic: {
        height: 60,
        width: 60,
        borderRadius: 60/2,
    },
    detailContainer: {
        width: 180,
        paddingLeft: 10,
    },
    historyName: {
        fontWeight: 'bold',
        fontSize: 16,
        color: '#404040'
    },
    historyDate: {
        fontSize: 13,
        color: '#C4C4C4',
    },
    historyNim: {
        fontSize: 13,
        fontWeight: 'bold',
        color: '#767676'
    },

    viewStatus: {
        width: 70,
        justifyContent: 'center',
        alignItems: 'center',
    },
    textStatus: {
        color: '#FEC427',
        fontSize: 13,
        fontWeight: 'bold',
    },
    titikTiga: {
        width: 7,
        justifyContent: 'center',
        alignItems: 'center',
        marginRight: 20,
    },
    boxPembeli: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        paddingBottom: 20
    },


});
