import React, {Component} from 'react';
import {
    FlatList,
    Image, StyleSheet, Text,
    TouchableOpacity, View,
} from 'react-native';
import Modal from 'react-native-modalbox';
import transferHistoryData from "../../../Data/transferHistoryData";


class FlatListItem extends Component {
    render() {
        return (
            <View>
                <View
                    style={styles.boxPembeli}>
                    <View>
                        <Image style={styles.whiteBoxPic} source={{uri: this.props.item.image}}/>
                    </View>

                    <View style={styles.detailContainer}>
                        <Text style={styles.historyName}>
                            {this.props.item.name}
                        </Text>
                        <Text style={styles.historyDate}>
                            {this.props.item.date}
                        </Text>
                        <Text style={styles.historyNim}>
                            {this.props.item.note}
                        </Text>
                    </View>


                    <View style={styles.viewStatus}>
                        <Text style={styles.textStatus}>
                            {this.props.item.status}
                        </Text>
                    </View>
                    <View style={styles.titikTiga}>
                        <Image source={require('../../../assets/icons/threePointsIcon.png')} />
                    </View>

                </View>

            </View>
        )
    }
}

export default class TransferHistoryPopUp extends Component {

    showHistory = () => {
        this.refs.historyPopup.open();
    };

    render() {
        return (
            <Modal
                ref={"historyPopup"}
                style={styles.popUp}
                position='center'
                backdrop={true}
                onClosed={() => {}}
            >
                <View style={styles.tobBar}>
                    <TouchableOpacity
                        style={styles.cancelButton}
                        onPress={() => {
                            this.refs.historyPopup.close()}}
                    >
                        <Image source={require('../../../assets/icons/blackCancelIcon.png')}/>
                    </TouchableOpacity>
                    <View style={styles.notificationTitle}>
                        <Image source={require('../../../assets/icons/historyIcon.png')}/>
                        <Text style={styles.tobBarText}>
                            History
                        </Text>
                    </View>
                </View>



                <FlatList
                    ref={'flatList'}
                    data={transferHistoryData}
                    keyExtractor={(item) => item.id}
                    renderItem={({item, index}) => (
                        <View>
                            <FlatListItem item={item} index={item} parentFlatList={this}/>
                        </View>

                    )}
                >

                </FlatList>
            </Modal>
        )
    }
}

const styles = StyleSheet.create({
    popUp:{
        width: 330,
        height: 420,
        alignItems: 'center',
        borderRadius: 10
    },
    tobBar: {
        padding: 20,
        flexDirection: 'row',
        alignItems: 'center',
        width: 330,
        height: 65,
        borderRadius: 10,
        backgroundColor: '#00D1FF',
        marginBottom: 10,
    },
    tobBarText:{
        color: '#FFFFFF',
        fontSize: 24,
    },
    cancelButton: {

    },
    notificationTitle: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
        paddingLeft: 60,

    },


});