import React, {Component} from 'react';
import {Button, Platform, StyleSheet, Text, View,
    TouchableOpacity, Image, Dimensions, ScrollView,
    ActivityIndicator, ImageBackground} from 'react-native';
import {Navigation} from "react-native-navigation";

var {height, width} = Dimensions.get('window');

export default class RegisterAndLogin extends Component<Props> {
    constructor(props) {
        super(props);
        Navigation.events().bindComponent(this);
    }





    render() {
    return (
        <ImageBackground source={require('../assets/img/prasmulPay_Background.png')} style={styles.backgroundImage}>
            <View style={styles.whiteOverlay}>
                <View style={styles.profilePicOutline}>
                    <Image source={require('../assets/img/sayako.jpg')} style={styles.profilePic}/>
                </View>
                <View style={styles.verifiedMerchant}>
                    <Image source={require('../assets/icons/verifiedIcon.png')}/>
                    <Text style={styles.verifiedMerchantText}>
                        VERIFIED MERCHANT
                    </Text>
                </View>

                <View style={styles.editBox}>
                    <Text style={styles.editBoxText}>
                        Sayako
                    </Text>
                </View>
                <View style={styles.editBox}>
                    <Text style={styles.editBoxText}>
                        sayako@nihon.go
                    </Text>
                </View>
                <View style={styles.editBox}>
                    <Text style={styles.editBoxText}>
                        ********
                    </Text>
                </View>
                <View style={styles.editBox}>
                    <Text style={styles.editBoxText}>
                        New Password
                    </Text>
                </View>
                <View style={styles.saveButton}>
                    <Text style={styles.saveButtonText}>
                        SAVE CHANGES
                    </Text>
                </View>
                <View style={styles.logOutButton}>
                    <Image source={require('../assets/icons/logOutIcon.png')}/>
                    <Text style={styles.logOutButtonText}>
                        Log Out
                    </Text>
                </View>

            </View>
        </ImageBackground>

    );
  }
}

const styles = StyleSheet.create({
    backgroundImage: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
    },
    whiteOverlay: {
        width: '100%',
        height: '100%',
        backgroundColor: 'rgba(255, 255, 255, .5)',
        alignItems: 'center',
        justifyContent: 'center',
    },
    profilePic: {
        height: 200,
        width: 200,
        borderRadius: 200/2
    },
    profilePicOutline: {
        height: 210,
        width: 210,
        borderRadius: 210/2,
        backgroundColor: '#FFFFFF',
        justifyContent: 'center',
        alignItems: 'center',
        margin: 5
    },
    verifiedMerchant: {
        flexDirection: 'row',
        margin: 5
    },
    verifiedMerchantText: {
        color: '#00D1FF',
        fontSize: 16,
    },
    editBox: {
        height: 55,
        width: 330,
        paddingLeft: 20,
        justifyContent: 'center',
        backgroundColor: '#F7F6F6',
        borderRadius: 10,
        margin: 10
    },
    editBoxText: {
        color: '#007EA7',
        fontSize: 16
    },
    saveButton: {
        width: 330,
        height: 40,
        borderRadius: 20,
        backgroundColor: '#007EA7',
        justifyContent: 'center',
        alignItems: 'center',
        marginVertical: 20
    },
    saveButtonText: {
        color: '#FFFFFF',
        fontSize: 18,
    },
    logOutButton: {
        flexDirection: 'row',
    },
    logOutButtonText: {
        color: '#C85F5F',
        fontSize: 18,
        fontWeight: 'bold'
    },



});
