import React, {Component} from 'react';
import {Button, Platform, StyleSheet, Text, View,
    TouchableOpacity, Image, Dimensions, ScrollView,
    ActivityIndicator, ImageBackground, FlatList, Alert} from 'react-native';
import {Navigation} from "react-native-navigation/lib/dist/index";
import AddItem from './AddItem';

let flatListFoodData = require('../../Data/flatListFoodData');

var {height, width} = Dimensions.get('window');

class FlatListItem extends Component {
    constructor(props) {
        super(props);
        this.state = {
            activeRowKey: null
        };
    }
    _updateItem = (screenName) => {
        Navigation.push(this.props.componentId, {
            component: {
                name: screenName
            }
        });

    };

    _deleteItem = () => {
        const deletingRow = this.state.activeRowKey;
        // console.log("Prec:", flatListFoodData);
        Alert.alert(
            'Alert',
            'Are you sure?',
            [
                {text: 'No', onPress: () => console.log('Cancel Pressed'), style: 'cancel'},
                {text: 'Yes', onPress: () => {

                        flatListFoodData = flatListFoodData.filter( (element) => element.key !== this.props.item.key);
                        // console.log(flatListFoodData);
                        //Refresh FlatList
                        this.props.parentFlatList.refreshFlatList()
                    }},

            ],
            {cancelable: true}
        );

    };

    render() {
        return (
            <View style={styles.whiteBoxMeals}>
                <View>
                    <Image style={styles.whiteBoxPic} source={{uri: this.props.item.image}}/>
                </View>

                <View style={styles.whiteBoxBotMenu}>
                    <Text style={styles.whiteBoxBotFood}>
                        {this.props.item.name}
                    </Text>
                    <Text>
                        {this.props.item.foodDesc}
                    </Text>
                    <Text>
                        {this.props.item.price}
                    </Text>
                </View>

                <View>
                    <TouchableOpacity
                        onPress={this._deleteItem}
                        style={styles.whiteBoxButtonBot}>
                        <Text style={styles.whiteBoxButtonTextBot}>
                            UPDATE
                        </Text>
                    </TouchableOpacity>
                </View>
            </View>
        )
    }
}

export default class EditPage extends Component<Props> {
    constructor(props) {
        super(props);
        this.state = ({
            deletedRowKey: null,
        });
        this._onPressAdd = this._onPressAdd.bind(this);
    }
    _onPressAdd () {
        // alert("You add item");
        this.refs.addItem.showAddItem();
    }
    refreshFlatList = (activeKey) => {
        console.log("State:", this.state.deletedRowKey);
        this.setState((prevState) => {
            console.log(prevState)
            return {
                deletedRowKey: activeKey
            };
        });
        this.refs.flatList.scrollToEnd();
    }

    render() {
    return (
        <ImageBackground source={require('../../assets/img/prasmulPay_Background.png')} style={styles.backgroundImage}>
            <View style={styles.whiteOverlay}>

                {/*
                ======>
                Kotak Putih Atas
                ======>
                */}

                <View style={styles.whiteBox}>
                    <View style={styles.whiteBoxText}>
                        <Text style={styles.whiteBoxTitle}>
                            Add New Meal
                        </Text>
                        <Text style={styles.whiteBoxDesc}>
                            Do you have any new Recipes or Meal?
                            Add your current Meals.
                        </Text>
                    </View>
                    <TouchableOpacity
                        onPress={this._onPressAdd}
                        style={styles.whiteBoxButton}>
                        <Text style={styles.whiteBoxButtonText}>
                            ADD
                        </Text>
                    </TouchableOpacity>
                </View>

                {/*
                ======>
                Kotak Putih Bawah
                ======>
                */}

                <View style={styles.whiteBoxBot}>

                    <Text style={styles.whiteBoxTitle}>
                        Your Meals
                    </Text>

                    <FlatList
                        ref={'flatList'}
                        data={flatListFoodData}
                        renderItem={ ( {item, index} ) => {
                            // console.log(`Item = ${JSON.stringify(item)}, index = ${index}`);
                            return (
                                <FlatListItem item={item} index={index} parentFlatList={this}/>
                            )
                        }}
                    />



                </View>
            </View>
            <AddItem ref={'addItem'} parentFlatList={this}>
            </AddItem>

        </ImageBackground>

    );
  }
}

const styles = StyleSheet.create({
    backgroundImage: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
    },
    whiteOverlay: {
        width: '100%',
        height: '100%',
        backgroundColor: 'rgba(255, 255, 255, .5)',
        alignItems: 'center',


    },
    whiteBox: {
        flexDirection: 'row',
        backgroundColor: '#FFFFFF',
        width: width,
        padding: 15,
        alignItems: 'center',
        justifyContent: 'space-between',
        marginBottom: 40
    },
    whiteBoxTitle: {
        fontSize: 18,
        paddingBottom: 5
    },
    whiteBoxDesc: {
        fontSize: 13,
        width: 186
    },
    whiteBoxButton: {
        backgroundColor: '#007EA7',
        borderRadius: 20,
        height: 40,
        width: 64,
        justifyContent: 'center',
        alignItems: 'center'
    },
    whiteBoxButtonText: {
        color: '#FFFFFF',
        fontSize: 18
    },
    whiteBoxBot: {
        backgroundColor: '#FFFFFF',
        width: width,
        padding: 15,
        marginBottom: 40
    },
    whiteBoxMeals: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        paddingBottom: 20
    },
    whiteBoxPic: {
        height: 60,
        width: 60,
    },
    whiteBoxBotMenu: {
        width: 200
    },
    whiteBoxBotFood: {
        fontWeight: 'bold'
    },

    whiteBoxButtonBot: {
        backgroundColor: '#007EA7',
        borderRadius: 20,
        height: 30,
        width: 70,
        justifyContent: 'center',
        alignItems: 'center',
    },
    whiteBoxButtonTextBot: {
        color: '#FFFFFF',
        fontSize: 13
    },

});
