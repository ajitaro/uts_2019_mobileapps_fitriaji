import React, {Component} from 'react';
import {Button, Platform, StyleSheet, Text, View,
    TouchableOpacity, Image, Dimensions, ScrollView,
    ActivityIndicator, ImageBackground, TextInput} from 'react-native';

import {Navigation} from "react-native-navigation/lib/dist/index";
import Modal from 'react-native-modalbox';

var {height, width} = Dimensions.get('window');

export default class AddItem extends Component {
    constructor(props) {
        super(props);
        this.state = {
            newFoodName: '',
            newFoodDesc: '',
            newFoodPrice: '',
        }
    }
    showAddItem = () => {
        this.refs.myItem.open();
    };

    generateKey = (numberOfCharacters) => {
        return require('random-string')({length: numberOfCharacters});
    }



    render() {
    return (
        <Modal
            ref={"myItem"}
            style={{
                justifyContent: 'center',
                height: '100%',
                width: '100%'

            }}
            position='center'
            backdrop={true}
            onClosed={() => {
                // alert("Modal closed");
            }}

        >
            <View style={styles.background}>
                <View style={styles.container}>
                    <View style={styles.tobBar}>
                        <TouchableOpacity
                            onPress={() => {
                                this.refs.myItem.close()}}
                        >
                            <Image source={require('../../assets/icons/cancelIcon.png')}/>
                        </TouchableOpacity>
                        <Text style={styles.tobBarText}>
                            Add Item
                        </Text>
                    </View>
                    <View style={styles.defaultMealPic}>
                        <Image source={require('../../assets/img/defaultMealPic.png')}/>
                    </View>
                    <View style={styles.bottomLine}>
                        <TextInput
                            onChangeText={(text) => this.setState({ newFoodName: text})}
                            placeholder={"Your Meal Name..."}
                            value={this.state.newFoodName}
                            style={{paddingBottom:10, fontSize: 20}}>
                        </TextInput>
                    </View>
                    <View>
                        <TextInput
                            onChangeText={(text) => this.setState({ newFoodDesc: text})}
                            placeholder={"Put your description here..."}
                            value={this.state.newFoodDesc}
                            style={[styles.bottomLine, {height: 100, fontSize: 14}]}>
                        </TextInput>
                    </View>
                    <View style={{width: width-40}}>
                        <TextInput
                            onChangeText={(text) => this.setState({ newFoodPrice: text})}
                            placeholder={"Rp."}
                            value={this.state.newFoodPrice}
                            style={{fontSize: 14}}>
                        </TextInput>
                    </View>

                    <TouchableOpacity
                        onPress={() => {
                            if (this.state.newFoodName.length == 0 || this.state.newFoodDesc.length == 0 || this.state.newFoodPrice.length == 0) {
                                alert("You must enter food's name, description, and price");
                            }
                            else {
                                const newKey = this.generateKey(24);
                                const newFood = {
                                    key: newKey,
                                    name: this.state.newFoodName,
                                    foodDesc: this.state.newFoodDesc,
                                    price: this.state.newFoodPrice,
                                    image: require('../../assets/img/sate.png'),
                                };
                                flatListFoodData.push(newFood);
                                this.props.parentFlatList.refreshFlatList(newKey);
                                this.refs.myItem.close();
                            }


                        }}
                        style={styles.addButton}>
                        <Text style={styles.addButtonText}>
                            Add
                        </Text>
                    </TouchableOpacity>





                </View>
            </View>
        </Modal>


    );
  }
}

const styles = StyleSheet.create({
    background: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
    },
    container: {
        width: '100%',
        height: '100%',
        alignItems: 'center',


    },
    tobBar: {
        width: width,
        backgroundColor: '#007EA7',
        padding: 10,
        justifyContent: 'center',

    },
    tobBarText: {
        color: '#FFFFFF',
        fontSize: 20,
        fontWeight: 'bold',
        textAlign: 'center'
    },
    defaultMealPic: {
        width: width-20,
        height: 200,
        backgroundColor: '#C4C4C4',
        justifyContent: 'center',
        alignItems: 'center',
        marginVertical: 20,
    },
    bottomLine: {
        width: width-40,
        borderBottomColor: '#C4C4C4',
        borderBottomWidth: 1,
        marginVertical: 10,
    },
    addButton: {
        width: 105,
        height: 35,
        backgroundColor: '#007EA7',
        borderRadius: 30,
        justifyContent: 'center',
        alignItems: 'center',
        alignSelf: 'flex-end',
        margin: 30

    },
    addButtonText:{
        fontSize: 21,
        fontWeight: 'bold',
        color: '#FFFFFF',

    }

});
