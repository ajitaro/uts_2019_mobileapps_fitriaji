import React, {Component} from 'react';
import {Button, Platform, StyleSheet, Text, View,
    TouchableOpacity, Image, Dimensions, ScrollView,
    ActivityIndicator, ImageBackground, Animated} from 'react-native';
import {Navigation} from "react-native-navigation";

const {height, width} = Dimensions.get('window');

export default class OrderPage extends Component {
    constructor(props) {
        super(props);
        Navigation.events().bindComponent(this);
    }

    // goToScreen = (screenName) => {
    //     Navigation.push(this.props.componentId, {
    //         component: {
    //             name: screenName
    //         }
    //     });
    //
    // };
    state = {
        active: 0,
        xTabNewOrder: 0,
        xTabOnProgress: 0,
        translateX: new Animated.Value(0),
        translateXTabNewOrder: new Animated.Value(0),
        translateXTabOnProgress: new Animated.Value(width),
        translateY: -1000
    };

    handleSlide = type => {
        let { active, xTabNewOrder, xTabOnProgress, translateX, translateXTabNewOrder, translateXTabOnProgress } = this.state;
        Animated.spring(translateX, {
            toValue: type,
            duration: 100,
        }).start();
        if (active === 0){
            Animated.parallel([
                Animated.spring(translateXTabNewOrder, {
                    toValue: 0,
                    duration: 100
                }).start(),
                Animated.spring(translateXTabOnProgress,{
                    toValue: width,
                    duration: 100
                }).start(),
            ])
        }else {
            Animated.parallel([
                Animated.spring(translateXTabNewOrder, {
                    toValue: -width,
                    duration: 100
                }).start(),
                Animated.spring(translateXTabOnProgress,{
                    toValue: 0,
                    duration: 100
                }).start(),
            ])
        }
    }


    render() {
        let {xTabNewOrder,
            xTabOnProgress,
            translateX,
            active,
            translateXTabOnProgress,
            translateXTabNewOrder,
            translateY
        } = this.state;
    return (
        <ImageBackground source={require('../assets/img/prasmulPay_Background.png')} style={styles.backgroundImage}>
            <View style={styles.whiteOverlay}>

                {/*
                =====>
                Tab Switch (New Order) (On Progress)
                =====>
                */}

                <View style={styles.tabSwitch}>
                    <Animated.View
                        style={{position: 'absolute',
                            width: '50%',
                            height: '100%',
                            top: 0,
                            left: 0,
                            backgroundColor: '#00D1FF',
                            borderRadius: height/2,
                            transform: [{
                            translateX
                            }]
                        }}
                    />
                    <TouchableOpacity
                        onPress={() => this.setState({active: 0}, () => this.handleSlide(xTabNewOrder))}
                        style={styles.newOrderSwitch}
                        onLayout={event => this.setState({xTabNewOrder: event.nativeEvent.layout.x})}
                    >
                        <Text style={[styles.switchText, {color: active === 0 ? '#003459' : '#00D1FF'}]}>
                            New Order
                        </Text>
                    </TouchableOpacity>
                    <TouchableOpacity
                        onPress={() => this.setState({active: 1}, () => this.handleSlide(xTabOnProgress))}
                        style={styles.onProgressSwitch}
                        onLayout={event => this.setState({xTabOnProgress: event.nativeEvent.layout.x})}
                    >
                        <Text style={[styles.switchText, {color: active === 1 ? '#003459' : '#00D1FF'}]}>
                            On Progress
                        </Text>
                    </TouchableOpacity>
                </View>

                {/*
                =====>
                New Order Tab
                =====>
                */}

                <ScrollView contentContainerstyle={styles.scrollViewStyle} showsVerticalScrollIndicator={false}>
                    <Animated.View
                        style={{
                            transform: [{
                                translateX: translateXTabNewOrder,
                            }]
                        }}
                        onLayout={event => this.setState({translateY: event.nativeEvent.layout.height})}
                    >
                        <View style={styles.containerScroll}>
                        <View style={styles.blueBoxNewOrder}>
                        <View style={styles.blueBoxPicAndText}>
                            <Image source={require('../assets/img/sayako.jpg')}
                                   style={styles.blueBoxPic}/>
                            <View style={styles.blueBoxText}>
                                <Text style={styles.blueBoxName}>Baihaqi Saputra</Text>
                                <View style={styles.blueBoxDesc}>
                                    <View>
                                        <Text style={styles.blueBoxDescText}>1 pcs</Text>
                                        <Text style={styles.blueBoxDescText}>1 pcs</Text>
                                        <Text style={styles.blueBoxDescText}>2 pcs</Text>
                                    </View>
                                    <View>
                                        <Text style={styles.blueBoxDescText}>Ayam Waito</Text>
                                        <Text style={styles.blueBoxDescText}>Nasi Merah</Text>
                                        <Text style={styles.blueBoxDescText}>Teh Sisir Gila Batu</Text>
                                    </View>
                                </View>
                            </View>
                        </View>
                        <Text style={styles.blueBoxRupiah}>Rp. 75.000,-</Text>
                        <Text style={styles.blueBoxButton}>ACCEPT</Text>
                        </View>
                        </View>
                    </Animated.View>

                    {/*
                =====>
                On Progress Tab
                =====>
                */}

                    <Animated.View style={{

                        transform: [
                            {
                                translateX: translateXTabOnProgress,
                            },
                            {
                                translateY: -translateY
                            }
                        ]
                    }}>
                        <View style={styles.containerScroll}>
                        <View style={styles.blueBoxOnProgress}>
                            <View style={styles.blueBoxPicAndText}>
                                <Image source={require('../assets/img/sayako.jpg')}
                                       style={styles.blueBoxPic}/>
                                <View style={styles.blueBoxText}>
                                    <Text style={styles.blueBoxName}>Sayako</Text>
                                    <View style={styles.blueBoxDesc}>
                                        <View>
                                            <Text style={styles.blueBoxDescText}>1 pcs</Text>
                                            <Text style={styles.blueBoxDescText}>1 pcs</Text>
                                            <Text style={styles.blueBoxDescText}>2 pcs</Text>
                                        </View>
                                        <View>
                                            <Text style={styles.blueBoxDescText}>Ayam Waito</Text>
                                            <Text style={styles.blueBoxDescText}>Nasi Merah</Text>
                                            <Text style={styles.blueBoxDescText}>Teh Sisir Gila Batu</Text>
                                        </View>
                                    </View>
                                </View>
                            </View>
                            <Text style={styles.blueBoxRupiah}>Rp. 75.000,-</Text>
                            <Text style={styles.blueBoxButton}>FINISH</Text>
                        </View>


                        <View style={styles.blueBoxOnProgress}>
                            <View style={styles.blueBoxPicAndText}>
                                <Image source={require('../assets/img/sayako.jpg')}
                                       style={styles.blueBoxPic}/>
                                <View style={styles.blueBoxText}>
                                    <Text style={styles.blueBoxName}>Okayas</Text>
                                    <View style={styles.blueBoxDesc}>
                                        <View>
                                            <Text style={styles.blueBoxDescText}>1 pcs</Text>
                                            <Text style={styles.blueBoxDescText}>1 pcs</Text>
                                            <Text style={styles.blueBoxDescText}>2 pcs</Text>
                                        </View>
                                        <View>
                                            <Text style={styles.blueBoxDescText}>Ayam Waito</Text>
                                            <Text style={styles.blueBoxDescText}>Nasi Merah</Text>
                                            <Text style={styles.blueBoxDescText}>Teh Sisir Gila Batu</Text>
                                        </View>
                                    </View>
                                </View>
                            </View>
                            <Text style={styles.blueBoxRupiah}>Rp. 105.000,-</Text>
                            <Text style={styles.blueBoxButton}>FINISH</Text>
                        </View>
                        </View>
                    </Animated.View>
                </ScrollView>




            </View>
        </ImageBackground>

    );
  }
}

const styles = StyleSheet.create({
    backgroundImage: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
    },
    whiteOverlay: {
        width: '100%',
        height: '100%',
        backgroundColor: 'rgba(255, 255, 255, .5)',
        alignItems: 'center',
    },
    containerScroll: {
        width: width,
        alignItems: 'center',
    },
    tabSwitch: {
        flexDirection: 'row',
        marginBottom: 20,
        height: 36,
        position: 'relative',
        backgroundColor: '#003459',
        borderRadius: height/2,
    },
    switchText: {
        fontSize: 24,

    },
    newOrderSwitch: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: height/2,
    },
    onProgressSwitch: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: height/2,
    },
    blueBoxNewOrder:{
        width: 340,
        borderRadius: 50,
        backgroundColor: '#003459',
        justifyContent: 'center',
        paddingHorizontal: 20,
        paddingVertical: 20,
        marginBottom: 20,
    },
    blueBoxOnProgress:{
        width: 340,
        borderRadius: 50,
        backgroundColor: '#003459',
        justifyContent: 'center',
        paddingHorizontal: 20,
        paddingVertical: 20,
        marginBottom: 20,
    },
    blueBoxPic: {
        height: 50,
        width: 50,
        borderRadius: 50/2
    },
    blueBoxDesc: {
        flexDirection: 'row',
    },
    blueBoxPicAndText: {
        flexDirection: 'row',
        paddingBottom: 10
    },
    blueBoxName: {
        color: '#FFFFFF',
        fontSize: 16,
        padding: 5
    },
    blueBoxDescText: {
        color: '#FFFFFF',
        fontSize: 12,
        paddingHorizontal: 15
    },
    blueBoxRupiah: {
        color: '#FFFFFF',
        fontSize: 20,
        paddingTop: 50,

    },
    blueBoxButton: {
        color: '#FFFFFF',
        fontSize: 12,
        fontWeight: 'bold',
        textAlign: 'right',
        paddingHorizontal: 30
    },


});
